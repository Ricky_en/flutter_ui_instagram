import "package:flutter/material.dart";

import '../widgets/ProfilePicure.dart';
import '../pages/InfoItem.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 0, 0, 0),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: const Color.fromARGB(255, 0, 0, 0),
        title: Row(
          children: const [
            Text(
              "rcky.en",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 23,
                  color: Color.fromARGB(255, 255, 255, 255)),
            ),
            Icon(Icons.arrow_drop_down,
                color: Color.fromARGB(255, 255, 255, 255))
          ],
        ),
        actions: [
          IconButton(
              onPressed: () {},
              icon: const Icon(Icons.add_box_outlined,
                  color: Color.fromARGB(255, 255, 255, 255))),
          IconButton(
              onPressed: () {},
              icon: const Icon(Icons.menu_outlined,
                  color: Color.fromARGB(255, 255, 255, 255)))
        ],
      ),
      body: ListView(children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            children: [
              const ProfilePicture(),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: const [
                    InfoItem("Postingan", "0"),
                    InfoItem("Pengikut", "97"),
                    InfoItem("Mengikuti", "94")
                  ],
                ),
              )
            ],
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        const Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Text(
              "Ricky Eko Novianto",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            )),
        const SizedBox(
          height: 4,
        ),
        const Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Text(
              "Perancang",
              style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),
            )),
        const SizedBox(
          height: 4,
        ),
        const Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Text(
              "Information Systems",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            )),
        const SizedBox(
          height: 15,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Container(
            height: 60,
            decoration: BoxDecoration(
                color: const Color.fromARGB(255, 54, 53, 53),
                borderRadius: BorderRadius.circular(10)),
            child: Column(children: const [
              Padding(
                padding: EdgeInsets.fromLTRB(10, 13, 200, 5),
                child: Text(
                  "Dasbor Profesional",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 15),
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 15, 7),
                child: Text(
                  "Fitur dan sumber informasi khusus untuk kreator",
                  style: TextStyle(color: Colors.grey),
                ),
              )
            ]),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: const EdgeInsets.fromLTRB(43, 10, 10, 0),
                  height: 35,
                  width: 180,
                  decoration: BoxDecoration(
                      color: const Color.fromARGB(255, 54, 53, 53),
                      borderRadius: BorderRadius.circular(10)),
                  child: const Text(
                    "Edit profil",
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(35, 10, 10, 0),
                  height: 35,
                  width: 180,
                  decoration: BoxDecoration(
                      color: const Color.fromARGB(255, 54, 53, 53),
                      borderRadius: BorderRadius.circular(10)),
                  child: const Text(
                    "Bagikan profil",
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                )
              ],
            )),
        const SizedBox(height: 15),
        Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 15, 7),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  Container(
                    height: 70,
                    width: 70,
                    margin: const EdgeInsets.only(right: 8, left: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      border: Border.all(color: Colors.white, width: 2),
                      color: Colors.black,
                    ),
                    child: IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          size: 40,
                          Icons.add,
                          color: Colors.white,
                        )),
                  ),
                  Container(
                    height: 70,
                    width: 70,
                    margin: const EdgeInsets.only(right: 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: const Color.fromARGB(255, 75, 74, 74),
                    ),
                  ),
                  Container(
                    height: 70,
                    width: 70,
                    margin: const EdgeInsets.only(right: 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: const Color.fromARGB(255, 75, 74, 74),
                    ),
                  ),
                  Container(
                    height: 70,
                    width: 70,
                    margin: const EdgeInsets.only(right: 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: const Color.fromARGB(255, 75, 74, 74),
                    ),
                  ),
                  Container(
                    height: 70,
                    width: 70,
                    margin: const EdgeInsets.only(right: 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: const Color.fromARGB(255, 75, 74, 74),
                    ),
                  ),
                  Container(
                    height: 70,
                    width: 70,
                    margin: const EdgeInsets.only(right: 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: const Color.fromARGB(255, 75, 74, 74),
                    ),
                  ),
                  Container(
                    height: 70,
                    width: 70,
                    margin: const EdgeInsets.only(right: 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: const Color.fromARGB(255, 75, 74, 74),
                    ),
                  ),
                ],
              ),
            ))
      ]),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: 4,
          iconSize: 32,
          selectedItemColor: Colors.white,
          unselectedItemColor: const Color.fromARGB(255, 255, 255, 255),
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                backgroundColor: Colors.black,
                icon: Icon(Icons.home_filled),
                label: "Home"),
            BottomNavigationBarItem(
                backgroundColor: Colors.black,
                icon: Icon(Icons.search_sharp),
                label: "Home"),
            BottomNavigationBarItem(
                backgroundColor: Colors.black,
                icon: Icon(Icons.add_box_outlined),
                label: "Home"),
            BottomNavigationBarItem(
                backgroundColor: Colors.black,
                icon: Icon(Icons.play_circle),
                label: "Home"),
            BottomNavigationBarItem(
                backgroundColor: Colors.black,
                icon: Icon(Icons.person_2_outlined),
                label: "Home")
          ]),
    );
  }
}
