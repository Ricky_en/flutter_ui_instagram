import "package:flutter/material.dart";

class ProfilePicture extends StatelessWidget {
  const ProfilePicture({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
            height: 107,
            width: 107,
            decoration: BoxDecoration(
                gradient: const LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.red, Colors.yellow]),
                borderRadius: BorderRadius.circular(60))),
        Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
                border: Border.all(
                    color: const Color.fromARGB(255, 0, 0, 0), width: 4),
                image: const DecorationImage(
                    image: NetworkImage(
                        "https://instagram.fsrg9-1.fna.fbcdn.net/v/t51.2885-19/289454112_747274076615791_8991566657604302711_n.jpg?stp=dst-jpg_s320x320&_nc_ht=instagram.fsrg9-1.fna.fbcdn.net&_nc_cat=107&_nc_ohc=GE-lKWxdW8kAX9b8wDc&edm=AOQ1c0wBAAAA&ccb=7-5&oh=00_AfCzAzlr8ThgjeShk7wsZcrxkatqV_RyffDIp9c07Sfj-w&oe=642B34D2&_nc_sid=8fd12b")),
                borderRadius: BorderRadius.circular(60))),
      ],
    );
  }
}
